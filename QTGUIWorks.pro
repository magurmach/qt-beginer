#-------------------------------------------------
#
# Project created by QtCreator 2014-06-19T08:54:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTGUIWorks
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    FindDialog.h

FORMS    += mainwindow.ui
